import { Fragment } from "react"
import MainHeader from "./main-header"
import classes from "./layout.module.css"

function Layout(props) {
    return (
        <Fragment>
            <MainHeader />
            {props.children}
        </Fragment>
    )
}

export default Layout
