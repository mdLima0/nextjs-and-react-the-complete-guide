import { useRouter } from "next/router";
import React, { Fragment, useEffect, useState } from "react";
import useSwr from "swr";
import EventList from "../../components/events/event-list";
import { getFilteredEvents } from "../../helpers/api-utils";
import ResultsTitle from "../../components/results-title/results-title";
import Button from "../../components/events/UI/button";
import ErrorAlert from "../../components/error-alert/error-alert";

function FilteredEventsPage(props) {
    const router = useRouter();

    const filterData = router.query.slug;

    const { data, error } = useSwr(
        "https://nextjs-course-project-default-rtdb.europe-west1.firebasedatabase.app/events.json"
    );

    const [loadedEvents, setLoadedEvents] = useState();

    useEffect(() => {
        if (data) {
            const events = [];

            for (const key in data) {
                events.push({
                    id: key,
                    ...data[key],
                });
            }

            setLoadedEvents(events);
        }
    }, [data]);

    if (!loadedEvents) {
        return <p className="center">Loading...</p>;
    }

    const filteredYear = +filterData[0];
    const filteredMonth = +filterData[1];

    if (
        isNaN(filteredYear) ||
        isNaN(filteredMonth) ||
        filteredYear > 2030 ||
        filteredMonth < 1 ||
        filteredMonth > 12 ||
        error
    ) {
        return (
            <Fragment>
                <ErrorAlert>
                    <p>Invalid Filter. Please Adjust your values</p>
                </ErrorAlert>
                <div className="center">
                    <Button link="/events">Show All Events</Button>
                </div>
            </Fragment>
        );
    }

    const filteredEvents = loadedEvents.filter((event) => {
        const eventDate = new Date(event.date);
        return (
            eventDate.getFullYear() === filteredYear &&
            eventDate.getMonth() === filteredMonth - 1
        );
    });

    if (!filteredEvents || filteredEvents.length === 0) {
        return (
            <Fragment>
                <ErrorAlert>
                    <p>No events found for the chosen filter</p>
                </ErrorAlert>
                <div className="center">
                    <Button link="/events">Show All Events</Button>
                </div>
            </Fragment>
        );
    }

    const date = new Date(filteredYear, filteredMonth - 1);

    return (
        <div>
            <ResultsTitle date={date} />
            <EventList items={filteredEvents} />
        </div>
    );
}

/* export async function getServerSideProps(context) {
    
    const params = context.params;

    const filterData = params.slug;
    console.log(filterData)

    const filteredYear = +filterData[0];
    const filteredMonth = +filterData[1];

    if (
        isNaN(filteredYear) ||
        isNaN(filteredMonth) ||
        filteredYear > 2030 ||
        filteredMonth < 1 ||
        filteredMonth > 12
    ) {
        return {
            props: {
                hasError: true
            }
           // notFound: true,
        };
    }

    const filteredEvents = await getFilteredEvents({
        year: filteredYear,
        month: filteredMonth,
    });

    
    return {
        props: {
            filteredEvents: filteredEvents,
            date: {
                year: filteredYear,
                month: filteredMonth
            }
        }
    }

} */

export default FilteredEventsPage;
