import { connectDatabase, insertDocument } from "../../helpers/db-utils";

async function handler(req, res) {
    if (req.method === "POST") {
        const userEmail = req.body.email;
        let client;

        if (!userEmail || !userEmail.includes("@")) {
            res.status(422).json({ message: "Invalid email address" });
            return;
        }

        try {
            client = await connectDatabase();
        } catch (err) {
            res.status(500).json({
                message: "Connecting to the DB failed! Please try again later!",
            });
            return;
        }

        try {
            await insertDocument(client, "newsletter", { email: userEmail });
            client.close();
        } catch (err) {
            res.status(500).json({ message: "Inserting data failed!" });
            return;
        }

        res.status(201).json({ message: `User ${userEmail} has signed Up` });
    }
}

export default handler;
