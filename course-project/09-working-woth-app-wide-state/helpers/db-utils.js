import { MongoClient } from "mongodb";

export async function connectDatabase() {
    return MongoClient.connect(process.env.DB_HOST);
}

export async function insertDocument(client, collection, document) {
    return client.db().collection(collection).insertOne(document);
}

export async function getAllDocuments(client, collection, sort) {
    return client.db().collection(collection).find().sort(sort).toArray();
}

export async function getAllFilteredDocuments(
    client,
    collection,
    sort,
    filter = {}
) {
    return client.db().collection(collection).find(filter).sort(sort).toArray();
}
