import classes from "./comment-list.module.css";

function CommentList(props) {
    const { commentList } = props;

    return (
        <ul className={classes.comments}>
            {commentList.map((comment) => (
                <li key={comment._id}>
                    <p>{comment.message}</p>
                    <div>
                        By <address>{comment.name}</address>
                    </div>
                </li>
            ))}
        </ul>
    );
}

export default CommentList;
