import {
    connectDatabase,
    getAllFilteredDocuments,
    insertDocument,
} from "../../../helpers/db-utils";

async function handler(req, res) {
    const eventId = req.query.eventId;
    let client;
    let result;

    try {
        client = await connectDatabase();
    } catch (err) {
        res.status(500).json({
            message: "Connecting to the DB failed! Please try again later!",
        });
        return;
    }

    if (req.method === "GET") {
        // get all comments by event
        try {
            result = await getAllFilteredDocuments(
                client,
                "comments",
                { _id: -1 },
                { eventId: eventId }
            );
        } catch (err) {
            res.status(500).json({ message: "Getting data failed!" });
            return;
        }

        res.status(200).json({ comments: result });
    }

    if (req.method === "POST") {
        // add server side validation
        const { email, name, text } = req.body;
        let result;

        if (
            !email.includes("@") ||
            !name ||
            name.trim() === "" ||
            !text ||
            text.trim() === ""
        ) {
            res.json(422).json({ message: "Invalid Input" });
            return;
        }

        const newComment = {
            email,
            name,
            message: text,
            eventId,
        };

        if (req.method === "GET") {
            // get all comments by event
            try {
                result = await insertDocument(client, "comments", newComment);
            } catch (err) {
                res.status(500).json({ message: "Inserting data failed!" });
                return;
            }

            newComment.id = result.insertedId;

            res.status(201).json({ message: "Added Comment", newComment });
        }

        client.close();
    }
}

export default handler;
