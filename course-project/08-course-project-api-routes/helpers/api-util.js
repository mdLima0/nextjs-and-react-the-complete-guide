export async function getAllEvents() {
    const response = await fetch(process.env.FIREBASE_API);
    const data = await response.json();

    const events = [];

    for (const key in data) {
        events.push({
            id: key,
            ...data[key],
        });
    }

    return events;
}

export async function getFeaturedEvents() {
    return getAllEvents().then((events) => {
        return events.filter((event) => event.isFeatured);
    });
}

export async function getEventById(id) {
    return getAllEvents().then((events) => {
        return events.find((event) => event.id === id);
    });
}

export async function getFilteredEvents(dateFilter) {
    const { year, month } = dateFilter;

    return getAllEvents().then((events) => {
        return events.filter((event) => {
            const eventDate = new Date(event.date);
            return (
                eventDate.getFullYear() === year &&
                eventDate.getMonth() === month - 1
            );
        });
    });
}
