import { MongoClient } from "mongodb";

export async function getClient() {
    return MongoClient.connect(process.env.DB_HOST);
}

export async function insertMessage(client, collection, document) {
    return client.db().collection(collection).insertOne(document);
}

export async function getAllMessages(client, collection, sort) {
    return client.db().collection(collection).find().sort(sort).toArray();
}
