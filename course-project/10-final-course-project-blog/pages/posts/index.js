import { Fragment } from "react";
import Head from "next/head";

import AllPosts from "../../components/posts/all-posts";
import { getAllPosts } from "../../utils/posts-util";

export function getStaticProps() {
    const postsList = getAllPosts();

    return {
        props: {
            posts: postsList,
        },
    };
}

function AllPostsPage(props) {
    return (
        <Fragment>
            <Head>
                <title>All Posts</title>
                <meta
                    name="description"
                    content="A list of all programming-related tutorials and posts!"
                />
            </Head>
            <AllPosts posts={props.posts} />
        </Fragment>
    );
}

export default AllPostsPage;
