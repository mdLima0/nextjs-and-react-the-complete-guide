import nodemailer from "nodemailer";
import { getClient, insertMessage } from "../../utils/db-util";

async function handler(req, res) {
    if (req.method === "POST") {
        const { email, name, message } = req.body;

        if (
            !email ||
            !email.includes("@") ||
            !name ||
            name.trim() === "" ||
            !message ||
            message.trim() === ""
        ) {
            res.status(422).json({ message: "Invalid input." });
            return;
        }

        let transporter = nodemailer.createTransport({
            service: "gmail",
            auth: {
                type: "OAuth2",
                user: process.env.MAIL_USERNAME,
                pass: process.env.MAIL_PASSWORD,
                clientId: process.env.OAUTH_CLIENT_ID,
                clientSecret: process.env.OAUTH_CLIENT_SECRET,
                refreshToken: process.env.OAUTH_REFRESH_TOKEN,
            },
        });

        let mailOptions = {
            from: `"${name} 👥" <${email}>`,
            to: `${process.env.MAIL_USERNAME}, ${process.env.MAIL_USERNAME}`,
            subject: `${message} ✔`,
            text: `${message} 🐴`,
            html: `<b>${message} 🐴</b>`,
        };

        transporter
            .sendMail(mailOptions)
            .then((response) => {
                res.status(201).json({
                    message: `Message successfully sent. Message ID: ${response.messageId}`,
                });
            })
            .catch((error) => {
                console.log(error);
                res.status(500).json({ message: "Internal server error." });
            });
    }
}

export default handler;
