import classes from "./logo.module.css";

function Logo() {
    return <div className={classes.logo}>Martim Lima's NextJs Blog</div>;
}

export default Logo;
