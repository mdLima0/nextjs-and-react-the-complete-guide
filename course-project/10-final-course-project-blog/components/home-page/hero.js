import Image from "next/image";

import classes from "./hero.module.css";

function Hero() {
    return (
        <section className={classes.hero}>
            <div className={classes.image}>
                <Image
                    src="/images/site/author.jpg"
                    alt="An image showing the author"
                    width={300}
                    height={300}
                />
            </div>
            <h1>Hi, I'm Martim</h1>
            <p>This blog was developed to learn more about NextJs</p>
        </section>
    );
}

export default Hero;
