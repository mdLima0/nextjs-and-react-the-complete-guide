import Link from "next/link";
import Image from "next/image";

import classes from "./post-item.module.css";

function PostItem(props) {
    const { title, image, excerpt, date, slug } = props.post;

    const fDate = new Date(date).toLocaleDateString("pt-PT", {
        day: "numeric",
        month: "long",
        year: "numeric",
    });

    const fImgPath = `/images/posts/${slug}/${image}`;

    const fLinkPath = `/posts/${slug}`;

    return (
        <li className={classes.post}>
            <Link href={fLinkPath}>
                <a>
                    <div className={classes.image}>
                        <Image
                            src={fImgPath}
                            alt={title}
                            width={300}
                            height={200}
                            layout="responsive"
                        />
                    </div>

                    <div className={classes.content}>
                        <h3>{title}</h3>
                        <time>{fDate}</time>
                        <p>{excerpt}</p>
                    </div>
                </a>
            </Link>
        </li>
    );
}

export default PostItem;
