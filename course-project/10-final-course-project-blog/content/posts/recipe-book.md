---
title: "Recipe Book App"
date: "2022-05-12"
image: "recipe-book.jpg"
excerpt: "Course Project developed during the VueJs Complete Guide"
isFeatured: true
---

# Recipe Book

This project was developed as the final course project of the Angular9 Complete Guide Course.



Check out the code [here](https://github.com/martimdLima/angular-9-the-complete-guide/tree/main/course_project) or try a live demo [here](https://ng-course-recipe-book-4d4d5.web.app/).