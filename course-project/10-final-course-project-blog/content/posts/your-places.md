---
title: "Your Places App"
date: "2022-05-12"
image: "your-places.jpeg"
excerpt: "Course Project developed during the MERN Complete Guide"
isFeatured: true
---

# Your Places App

This project was developed as the final course project of the MERN Complete Fullstack Guide Course.



Check out the code [here](https://github.com/martimdLima/mern-fullstack-guide/tree/master/final-mern-project) or try a live demo [here](https://mern-stack-course-292922.web.app/).