---
title: "Find a Coach App"
date: "2022-05-12"
image: "find-coach.png"
excerpt: "Course Project developed during the VueJs Complete Guide"
isFeatured: true
---

# Find a Coach

This project was developed as the final course project of the VueJS Complete Guide Course.



Check out the code [here](https://github.com/martimdLima/Vuejs---The-Complete-Guide/tree/master/course-project/vue3/03-coach-finder-starting-setup) or try a live demo [here](https://vuejs-course-d14c4.web.app/coaches).