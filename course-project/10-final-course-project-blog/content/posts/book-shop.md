---
title: "Book Shop"
date: "2022-05-12"
image: "book-shop.jpeg"
excerpt: "Course Project developed during the VueJs Complete Guide"
isFeatured: true
---

# Book Shop App

This project was developed as the final course project of the NodeJs Complete Guide Course.



Check out the code [here](https://github.com/martimdLima/nodejs-the-complete-guide-2020/tree/main/course_modules/deploying-the-app) or try a live demo [here](https://node-complete-guide-2020.herokuapp.com/).