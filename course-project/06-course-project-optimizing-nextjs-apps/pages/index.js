import { getFeaturedEvents } from "../helpers/api-utils";
import EventList from "../components/events/event-list";
import Head from "next/head";

function HomePage(props) {
    //const featuredEvents = getFeaturedEvents();

    return (
        <div>
            <Head>
                <title>NextJS Events</title>
                <meta
                    name="description"
                    content="A curated list of events for your tastes"
                />
            </Head>
            <EventList items={props.featuredEvents} />
        </div>
    );
}

export async function getStaticProps() {
    const featuredEvents = await getFeaturedEvents();

    return {
        props: {
            featuredEvents: featuredEvents,
        },
        revalidate: 1800,
    };
}

export default HomePage;
