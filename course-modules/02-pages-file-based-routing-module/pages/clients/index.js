import Link from "next/link";

function ClientsPage() {
    const clients = [
        { id: 1, name: "John Doe" },
        { id: 2, name: "Mary Sue" },
        { id: 3, name: "Shelly Owens" },
    ];

    return (
        <div>
            <ul>
                {clients.map((client) => (
                    <li key={client.id}>
                        <Link
                            href={{
                                pathname: "/clients/[id]",
                                query: { id: client.id },
                            }}>
                            {client.name}
                        </Link>
                    </li>
                ))}
            </ul>
        </div>
    );
}

export default ClientsPage;
