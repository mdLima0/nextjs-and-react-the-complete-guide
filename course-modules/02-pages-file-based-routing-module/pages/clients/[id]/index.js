import { useRouter } from "next/router";

function ClientProjectsPage() {
    const router = useRouter();

    const pathName = router.pathname;
    const query = router.query;

    function loadProjectHandler() {
        router.push({
            pathname: "/clients/[id]/[clientprojectid]",
            query: { id: 1, clientprojectid: "projecta" },
        })
    }

    return (
        <div>
            <h1>Projects for a given Client {query.id}</h1>
            <button onClick={loadProjectHandler}>Load Project A</button>
        </div>
    );
}

export default ClientProjectsPage;
