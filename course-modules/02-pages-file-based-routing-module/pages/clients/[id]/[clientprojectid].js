import { useRouter } from "next/router";

function SelectedClientProjectPage() {
    const router = useRouter();

    const pathName = router.pathname;
    const query = router.query;

    return (
        <div>
            <h1>
                The project page for a specific project for a selected Client {query.clientprojectid}
            </h1>
        </div>
    );
}

export default SelectedClientProjectPage;
