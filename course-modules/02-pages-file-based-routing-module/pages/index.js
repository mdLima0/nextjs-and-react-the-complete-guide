import Link from "next/link";
import { getFeaturedEvents } from "../../file-based-routing/dummy-data";

function HomePage() {

    const featuredEvents = getFeaturedEvents();

    return (
        <div>
            <h1>Home Page</h1>
            <ul>
                
            </ul>
        </div>
    );
}

export default HomePage;
