import { useRouter } from "next/router";

function PortefolioProjectPage() {
    const router = useRouter();

    const pathName = router.pathname;
    const query = router.query;
    console.log(pathName);
    console.log(query);

    return (
        <div>
            <h1>Portefio Project {query.id}</h1>
        </div>
    );
}

export default PortefolioProjectPage;
