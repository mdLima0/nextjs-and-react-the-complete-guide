import { useRouter } from "next/router";

function BlogPostsPage() {
    const router = useRouter();

    const pathName = router.pathname;
    const query = router.query;
    console.log(pathName);
    console.log(query);

    return (
        <div>
            <h1>Blog Posts Page {query.slug}</h1>
        </div>
    );
}

export default BlogPostsPage;
