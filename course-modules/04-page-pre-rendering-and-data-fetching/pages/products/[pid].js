import path from "path";
import fs from "fs/promises";

import { Fragment } from "react";

function ProductDetailPage(props) {
    const { loadedProduct } = props;

    if (!loadedProduct) {
        return <p>Loading...</p>;
    }

    return (
        <Fragment>
            <h1>{loadedProduct.title} </h1>
            <p>{loadedProduct.description} </p>
        </Fragment>
    );
}

async function dataHandler() {
    const filePath = path.join(process.cwd(), "data", "dummy-backend.json");
    const jsonData = await fs.readFile(filePath);
    const data = JSON.parse(jsonData);

    return data;
}

// "getStaticProps" for Dynamic Parameters
export async function getStaticProps(context) {
    const { params } = context;

    const productId = params.pid;

    const data = await dataHandler();

    const product = data.products.find((prod) => prod.id === productId);

    // Fallback Pages & "Not Found" Pages
    if (!product) {
        return { notFound: true };
    }

    return { props: { loadedProduct: product } };
}

//"getStaticPaths" For Dynamic Pages
export async function getStaticPaths() {
    const data = await dataHandler();

    const ids = data.products.map((product) => product.id);

    const pathsWithParams = ids.map((id) => ({ params: { pid: id } }));

    return {
        paths: pathsWithParams,
        fallback: false,
    };
}

export default ProductDetailPage;
