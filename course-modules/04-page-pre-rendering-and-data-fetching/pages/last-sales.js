import { useEffect, useState } from "react";
import useSWR from "swr";

function LastSalesPage() {
    const [sales, setSales] = useState();
    //const [loading, setLoading] = useState(false);

    // "useSWR" NextJS Hook
    const { data, error } = useSWR(
        "https://nextjs-course-project-default-rtdb.europe-west1.firebasedatabase.app/sales.json"
    );

    useEffect(() => {
        if (data) {
            const transformedSales = [];

            for (const key in data) {
                transformedSales.push({
                    id: key,
                    username: data[key].username,
                    volume: data[key].volume,
                });
            }

            setSales(transformedSales);
        }
    }, [data]);

    /*       useEffect(() => {
        setLoading(true);
        fetch('https://nextjs-course-project-default-rtdb.europe-west1.firebasedatabase.app/sales.json')
          .then((response) => response.json())
          .then((data) => {
            const transformedSales = [];
    
            for (const key in data) {
              transformedSales.push({
                id: key,
                username: data[key].username,
                volume: data[key].volume,
              });
            }
    
            setSales(transformedSales);
            setLoading(false);
          });
      }, []);
      */

    if (error) {
        return <p>Failed to load.</p>;
    }

    if (!data || !sales) {
        return <p>Loading...</p>;
    }

    return (
        <ul>
            {sales.map((sale) => (
                <li key={sale.id}>
                    {sale.username} - ${sale.volume}
                </li>
            ))}
        </ul>
    );
}

// Combining Pre-Fetching With Client-Side Fetching
export async function getStaticProps() {
    const response = await fetch(
        "https://nextjs-course-project-default-rtdb.europe-west1.firebasedatabase.app/sales.json"
    );
    const data = await response.json();

    const transformedSales = [];

    for (const key in data) {
        transformedSales.push({
            id: key,
            username: data[key].username,
            volume: data[key].volume,
        });
    }

    return { props: { sales: transformedSales } };
}

export default LastSalesPage;
